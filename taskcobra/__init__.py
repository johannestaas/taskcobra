#!/usr/bin/env python

# Dude I'm making this for: http://www.reddit.com/user/porlov
# Requested these features:
# support for regular (one-time) and recurring (with different granularity
# like daily, weekly, monthly etc) tasks
# support for tagging tasks with keywords
# support for dependencies between tasks (task gets locked until all
# dependencies are completed)
# optional support for nice colored reports like these
# http://images.51cto.com/files/uploadimg/20120711/14003527.png

