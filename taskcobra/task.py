#!/usr/bin/env python
import bisect

class TaskList(object):
    ''' List of tasks, to be serialized into file storage
    '''

    def __init__(self, name):
        self.name = name
        self.tasks = []
        self.completed = 0
        # Add 1000 to index here to get the custom tag
        self.custom_tags = []
        self.counted_id = 0

    def add(self, task):
        bisect.insort(self.tasks, (task.priority, task))

    def done(self, num):
        self.completed += 1
        self.remove(num)

    def remove(self, num):
        del self.tasks[num]

    def serialize(self):
        return {
            'name': self.name,
            'completed': self.completed,
            'tasks': [task.serialize() for p, task in self.tasks],
            'custom_tags': self.custom_tags,
            'counted_id': self.counted_id,
        }
    
    def deserialize(self, d):
        self.name = d['name']
        self.completed = d['completed']
        self.custom_tags = d['custom_tags']
        self.counted_id = d['counted_id']
        Counted.ID = self.counted_id
        self.tasks = []
        for dtask in d['tasks']:
            task = dtask.deserialize()
            self.add(task)
        
class Counted(object):
    ID = 0
    
    def __init__(self):
        self.id = Counted.ID
        Counted.ID += 1

class Task(Counted):
    ''' Task with description and priority 
    '''
    # Priorities
    PRIORITY = ['Top', 'High', 'Medium', 'Low', 'None']
    TOP, HIGH, MED, LOW, NONE = range(5)
    # Task keywords and tags
    TAGS = ['Personal', 'Work', 'Medical', 'Financial', 'Household']
    # How often it recurs
    RECUR = ['Hours', 'Days', 'Weeks', 'Months', 'Years']
    HOURS, DAYS, WEEKS, MONTHS, YEARS = range(5)

    def init(self, desc=None, due=None, frequency=0, recurrence=None,
        priority=MED, tags=None):
        ''' Create task with description.

        :param str desc: General description
        :param `datetime.datetime` due: due date
        :param int frequency: how often it occurs. 0 for never
        :param int recurrence: recurs every ``frequency`` RECUR[recurrence]
        :param int priority: Priority, 0 being highest
        :param list tags: list of keywords it's tagged with, as integers
        '''
        super(Task, self).__init__()
        self.desc = desc
        self.tags = tags or []
        self.priority = priority
        self.due = due

    def serialize(self):
        ''' Serialize into dict

        :return dict: A dictionary describing all its attributes
        '''
        return {
            'id': self.id,
            'desc': self.desc,
            'tags': self.tags,
            'priority': self.priority,
        }

    def deserialize(self, d):
        self.id = d['id']
        self.desc = d['desc']
        self.tags = d['tags']
        self.priority = d['priority']
