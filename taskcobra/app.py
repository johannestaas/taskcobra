#!/usr/bin/env python
import json
from glob import glob
from shellify import Shell

from task import TaskList, Task

shell = Shell('TaskCobra', version='0.0.2')

LOADED_LISTS = []
SELECTED_LIST = None

@shell
def load(path):
    ''' Load task list at path (wildcard valid) '''
    global LOADED_LISTS
    paths = glob(path)
    if not paths:
        print('No task list found.')
        return
    loaded = 0
    for path in paths:
        with open(path) as f:
            serialized = json.load(f)           
        tasklist = TaskList()
        tasklist.deserialize(serialized)
        LOADED_LISTS += [tasklist]
            
@shell
def new(name):
    ''' Create new task list '''
    global SELECTED_LIST
    global LOADED_LISTS
    tasklist = TaskList(name)
    SELECTED_LIST = tasklist
    LOADED_LISTS += [tasklist]
    with open(name + '.json', 'w') as f:
        serialized = tasklist.serialize()
        json.dump(serialized, f, indent=4)
    print('Created and selected new tasklist.')

shell.run()
