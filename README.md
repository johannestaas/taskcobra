taskcobra
=========

Organize and keep track of your tasks, and mark them off

##### Emphasis on the following subset of features:
- regular (one-time) and recurring tasks
    - hourly, daily, weekly, monthly (always, once, X times or until T)
- tagging tasks with keywords
- support for dependencies between tasks 
  (task gets locked until all dependencies are completed)
- optional support for nice colored reports
    - http://images.51cto.com/files/uploadimg/20120711/14003527.png
